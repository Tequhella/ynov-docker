GCC = g++

CFLAGS = -Wall -g `pkg-config --cflags gtkmm-3.0`

CLIBS = `pkg-config --libs gtkmm-3.0`

OBJ = build/

SRC = sources/

HEAD = headers/

all: $(OBJ)main.o $(OBJ)perso.o $(OBJ)interface.o $(OBJ)barreProgression.o
	$(GCC) $(CFLAGS) $(OBJ)main.o $(OBJ)perso.o $(OBJ)interface.o $(OBJ)barreProgression.o -o ynov-docker $(CLIBS)

build/main.o: $(SRC)main.cpp  $(HEAD)perso.h $(HEAD)interface.h
	$(GCC) $(CFLAGS) -c $(SRC)main.cpp -o $(OBJ)main.o

build/perso.o: $(SRC)perso.cpp $(HEAD)perso.h
	$(GCC) $(CFLAGS) -c $(SRC)perso.cpp -o $(OBJ)perso.o

build/interface.o: $(SRC)interface.cpp $(HEAD)interface.h
	$(GCC) $(CFLAGS) -c $(SRC)interface.cpp -o $(OBJ)interface.o

build/barreProgression.o: $(SRC)barreProgression.cpp $(HEAD)barreProgression.h
	$(GCC) $(CFLAGS) -c $(SRC)barreProgression.cpp -o $(OBJ)barreProgression.o

clean:
	rm -f ynov-docker $(OBJ)*.o