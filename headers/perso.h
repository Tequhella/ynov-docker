/**
 * @file xp.h
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief 
 * @version 0.1
 * @date 17-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef XP_H
#define XP_H

#include <iostream>
#include <string>
#include <string.h>
using namespace std;

class Perso
{
    private:
        string name;
        int level;
        int max_xp;
        int current_xp;

    public:

        Perso();
        Perso(string name);
        Perso(string name, int level, int max_xp, int current_xp);
        ~Perso();

        /**
         * @brief Retourne le nom du personnage.
         * 
         * @return une chaîne de caractères correspondant au nom du personnage.
         */
        string getName();

        /**
         * @brief Retourne le niveau.
         * 
         * @return un entier correspondant au niveau.
         */
        int getLevel();

        /**
         * @brief Retourne le nombre maximum d'expérience du niveau.
         * 
         * @return un entier correspondant au nombre maximum d'expérience.
         */
        int getMaxXp();

        /**
         * @brief Retourne l'expérience actuelle.
         * 
         * @return un entier correspondant à l'expérience actuelle.
         */
        int getCurrentXp();

        /**
         * @brief Modifie le nom du personnage.
         * 
         * @param name une chaîne de caractères correspondant au nouveau nom du personnage.
         */
        void setName(string name);

        /**
         * @brief Modifie le niveau.
         * 
         * @param level un entier correspondant au nouveau niveau.
         */
        void setLevel(int level);

        /**
         * @brief Modifie le nombre maximum d'expérience du niveau.
         * 
         * @param max_xp un entier correspondant au nouveau nombre maximum d'expérience.
         */
        void setMaxXp(int max_xp);

        /**
         * @brief Modifie l'expérience actuelle.
         * 
         * @param current_xp un entier correspondant à la nouvelle expérience actuelle.
         */
        void setCurrentXp(int current_xp);

    

        /**
         * @brief Ajoute de l'expérience.
         * 
         * @param xp un entier correspondant à l'expérience à ajouter.
         */
        void addXp(int xp);

        /**
         * @brief Enlève de l'expérience.
         * 
         * @param xp un entier correspondant à l'expérience à enlever.
         */
        void removeXp(int xp);

        /**
         * @brief Augmente le niveau.
         * 
         */
        void levelUp();

        /**
         * @brief Diminue le niveau.
         * 
         */
        void levelDown();

        /**
         * @brief Affiche les informations de l'expérience.
         * 
         */
        void displayPerso();
};

#endif