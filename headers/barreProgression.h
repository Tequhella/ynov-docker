/**
 * @file barreProgression.h
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief 
 * @version 0.1
 * @date 26-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef BARREPROGRESSION_H
#define BARREPROGRESSION_H

#define BORDER_WIDTH 2


#include <gtkmm-3.0/gtkmm.h>
#include <cairomm/cairomm.h>

using namespace Gtk;
using namespace Cairo;

class BarreProgression : public DrawingArea
{
    private:
        int width;  // Largeur de la barre
        int height; // Hauteur de la barre
        int value;  // Valeur de la barre
        int maxValue;   // Valeur maximale de la barre

    protected:

        /**
         * @brief Dessine la barre de progression.
         * 
         * @param cr correspondant au contexte de dessin.
         */
        bool on_draw(const RefPtr<Context>& cr) override;

    public:

        /****************/
        /* Constructeur */
        /****************/

        /**
         * @brief Constructeur par défaut.
         * 
         */
        BarreProgression();

        /**
         * @brief Constructeur avec largeur et hauteur.
         * 
         * @param width Largeur de la barre.
         * @param height Hauteur de la barre.
         */
        BarreProgression(int width, int height);

        /**
         * @brief Constructeur avec largeur, hauteur, valeur et valeur maximale.
         * 
         * @param width Largeur de la barre.
         * @param height Hauteur de la barre.
         * @param value Valeur de la barre.
         * @param maxValue Valeur maximale de la barre.
         */
        BarreProgression(int width, int height, int value, int maxValue);

        /**
         * @brief Destructeur.
         * 
         */
        ~BarreProgression();

        /************/
        /* Méthodes */
        /************/
        
        /**
         * @brief Actualise la barre de progression.
         * 
         * @param value Nouvelle valeur de la barre.
         */
        void update(int value, int maxValue);

        /***********/
        /* GETTERS */
        /***********/

        /**
         * @brief Retourne la largeur de la barre.
         * 
         * @return int correspondant à la largeur de la barre.
         */
        int getWidth() const;

        /**
         * @brief Retourne la hauteur de la barre.
         * 
         * @return int correspondant à la hauteur de la barre.
         */
        int getHeight() const;

        /**
         * @brief Retourne la valeur de la barre.
         * 
         * @return int correspondant à la valeur de la barre.
         */
        int getValue() const;

        /**
         * @brief Retourne la valeur maximale de la barre.
         * 
         * @return int correspondant à la valeur maximale de la barre.
         */
        int getMaxValue() const;

        /***********/
        /* SETTERS */
        /***********/

        /**
         * @brief Modifie la largeur de la barre.
         * 
         * @param width correspondant à la nouvelle largeur de la barre.
         */
        void setWidth(int width);

        /**
         * @brief Modifie la hauteur de la barre.
         * 
         * @param height correspondant à la nouvelle hauteur de la barre.
         */
        void setHeight(int height);

        /**
         * @brief Modifie la valeur de la barre.
         * 
         * @param value correspondant à la nouvelle valeur de la barre.
         */
        void setValue(int value);

        /**
         * @brief Modifie la valeur maximale de la barre.
         * 
         * @param maxValue correspondant à la nouvelle valeur maximale de la barre.
         */
        void setMaxValue(int maxValue);

        /**
         * @brief Modifie la zone de dessin.
         * 
         * @param drawingArea correspondant à la nouvelle zone de dessin.
         */
        void setDrawingArea(DrawingArea drawingArea);
};

#endif // BARREPROGRESSION_H