/**
 * @file interface.h
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief Affichage des informations du personnage.
 * @version 0.1
 * @date 17-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <gtkmm-3.0/gtkmm.h>
#include <cairomm/cairomm.h>
#include "../headers/perso.h"
#include "../headers/barreProgression.h"

using namespace Gtk;
using namespace Cairo;

class Interface
{
    private:
        int width;  // Largeur de la fenêtre
        int height; // Hauteur de la fenêtre

        Window window;           // Fenêtre
        Box box;                 // Boîte verticale
        Box boxButton;           // Boîte horizontale
        Label labelName;         // Label pour afficher le nom du personnage
        Label labelLevel;        // Label pour afficher le niveau du personnage
        Label labelXp;           // Label pour afficher l'expérience du personnage
        Button buttonAjoutXp;    // Bouton pour ajouter de l'expérience
        Button buttonRetraitXp;  // Bouton pour retirer de l'expérience
        DrawingArea drawingArea; // Zone de dessin

        BarreProgression* barreProgression; // Barre de progression
        Perso* perso;                       // Pointeur vers le personnage
        
    public:
        Interface();
        Interface(Perso& perso);
        Interface(int width, int height);
        Interface(int width, int height, Perso& perso);
        ~Interface();

        /**
         * @brief Retourne la largeur de la fenêtre.
         * 
         * @return int correspondant à la largeur de la fenêtre.
         */
        int getWidth();

        /**
         * @brief Retourne la hauteur de la fenêtre.
         * 
         * @return int correspondant à la hauteur de la fenêtre.
         */
        int getHeight();

        /**
         * @brief Modifie la largeur de la fenêtre.
         * 
         */
        void setWidth(int width);

        /**
         * @brief Modifie la hauteur de la fenêtre.
         * 
         */
        void setHeight(int height);

        /**
         * @brief Dessine la barre de progression.
         * 
         */
        void drawBarreProgression();
        
        /**
         * @brief Ajoute de l'expérience au personnage.
         * 
         */
        void clickAjoutXp();

        /**
         * @brief Enlève de l'expérience au personnage.
         * 
         */
        void clickEnleverXp();

        /**
         * @brief Affiche la fenêtre.
         * 
         */
        void afficher();
        
};


#endif // INTERFACE_H