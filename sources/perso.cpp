/**
 * @file xp.cpp
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief 
 * @version 0.1
 * @date 17-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "../headers/perso.h"

Perso::Perso() : name("Default"), level(0), max_xp(100), current_xp(0){}

Perso::Perso(string name) : name(name), level(0), max_xp(0b1100100), current_xp(0){}

Perso::Perso(string name, int level, int max_xp, int current_xp) : name(name), level(level), max_xp(max_xp), current_xp(current_xp){}

Perso::~Perso(){}

string Perso::getName()
{
    return this->name;
}

int Perso::getLevel()
{
    return level;
}

int Perso::getMaxXp()
{
    return max_xp;
}

int Perso::getCurrentXp()
{
    return current_xp;
}

void Perso::setName(string name)
{
    this->name = name;
}

void Perso::setLevel(int level)
{
    this->level = level;
}

void Perso::setMaxXp(int max_xp)
{
    this->max_xp = max_xp;
}

void Perso::setCurrentXp(int current_xp)
{
    this->current_xp = current_xp;
}

void Perso::addXp(int xp)
{
    current_xp += xp;
    if (current_xp >= max_xp)
    {
        current_xp -= max_xp;
        levelUp();
    }
}

void Perso::removeXp(int xp)
{
    current_xp -= xp;
    if (current_xp < 0)
    {
        levelDown();
        current_xp += max_xp;
    }
}

void Perso::levelUp()
{
    level++;
    max_xp *= 1.25;
    cout << "Vous avez gagné un niveau !" << endl;
}

void Perso::levelDown()
{
    level--;
    max_xp /= 1.25;
    cout << "Vous avez perdu un niveau !" << endl;
}

void Perso::displayPerso()
{
    cout << "==========================" << endl;
    cout << "Nom : " << name << endl;
    cout << "Niveau : " << level << endl;
    cout << "Expérience : " << current_xp << "/" << max_xp << endl;
    cout << current_xp << " ";
    for (uint8_t i = 0; i < 20; i++)
    {
        if (i < (current_xp * 20) / max_xp) /*--->*/ cout << "█";
        else /*---------------------------------->*/ cout << " ";
    }
    cout << " " << max_xp << endl;

    cout << "==========================" << endl;
}

