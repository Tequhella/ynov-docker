/**
 * @file interface.cpp
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief Affichage des informations du personnage.
 * @version 0.1
 * @date 18-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "../headers/interface.h"

Interface::Interface() : width(800), height(600), perso(nullptr)
{
    // Initialisation de la fenêtre
    window.set_title("Interface");
    window.set_default_size(width, height);
    window.set_position(Gtk::WIN_POS_CENTER);
    window.set_border_width(10);

    // Initialisation de la boîte verticale
    box.set_orientation(Gtk::ORIENTATION_VERTICAL);
    box.set_spacing(10);
    window.add(box);

    // Initialisation de la boîte horizontale
    boxButton.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    boxButton.set_spacing(10);

    // Initialisation des labels pour afficher les informations du personnage
    Perso perso("Default", 1, 100, 0);
    labelName.set_text("Nom : " + perso.getName());
    labelLevel.set_text("Niveau : " + to_string(perso.getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso.getCurrentXp()) + "/" + to_string(perso.getMaxXp()));
    box.pack_start(labelName);
    box.pack_start(labelLevel);
    box.pack_start(labelXp);

    // Initialisation des boutons dans la boîte horizontale
    buttonAjoutXp.set_label("Ajouter de l'expérience");
    buttonAjoutXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickAjoutXp));
    boxButton.pack_start(buttonAjoutXp);

    buttonRetraitXp.set_label("Retirer de l'expérience");
    buttonRetraitXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickEnleverXp));
    boxButton.pack_start(buttonRetraitXp);

    // Ajout des boutons à la boîte verticale
    box.pack_start(boxButton);
}

Interface::Interface(Perso& perso) : width(800), height(600), perso(&perso)
{
    // Initialisation de la fenêtre
    window.set_title("Interface");
    window.set_default_size(width, height);
    window.set_position(Gtk::WIN_POS_CENTER);
    window.set_border_width(10);

    // Initialisation de la boîte verticale
    box.set_orientation(Gtk::ORIENTATION_VERTICAL);
    box.set_spacing(10);
    window.add(box);

    // Initialisation de la boîte horizontale
    boxButton.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    boxButton.set_spacing(10);

    // Initialisation des labels pour afficher les informations du personnage
    labelName.set_text("Nom : " + perso.getName());
    labelLevel.set_text("Niveau : " + to_string(perso.getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso.getCurrentXp()) + "/" + to_string(perso.getMaxXp()));
    box.pack_start(labelName);
    box.pack_start(labelLevel);
    box.pack_start(labelXp);

    // Initialisation des boutons dans la boîte horizontale
    buttonAjoutXp.set_label("Ajouter de l'expérience");
    buttonAjoutXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickAjoutXp));
    boxButton.pack_start(buttonAjoutXp);

    buttonRetraitXp.set_label("Retirer de l'expérience");
    buttonRetraitXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickEnleverXp));
    boxButton.pack_start(buttonRetraitXp);

    // Ajout des boutons à la boîte verticale
    box.pack_start(boxButton);

    // Initialisation de la barre de progression
    barreProgression = new BarreProgression();

    // Ajout de la barre de progression à la boîte verticale
    box.pack_start(*barreProgression);
}

Interface::Interface(int width, int height) : width(width), height(height), perso(nullptr)
{
    // Initialisation de la fenêtre
    window.set_title("Interface");
    window.set_default_size(width, height);
    window.set_position(Gtk::WIN_POS_CENTER);
    window.set_border_width(10);

    // Initialisation de la boîte verticale
    box.set_orientation(Gtk::ORIENTATION_VERTICAL);
    box.set_spacing(10);
    window.add(box);

    // Initialisation de la boîte horizontale
    boxButton.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    boxButton.set_spacing(10);

    // Initialisation des labels pour afficher les informations du personnage
    Perso perso("Default", 1, 100, 0);
    labelName.set_text("Nom : " + perso.getName());
    labelLevel.set_text("Niveau : " + to_string(perso.getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso.getCurrentXp()) + "/" + to_string(perso.getMaxXp()));
    box.pack_start(labelName);
    box.pack_start(labelLevel);
    box.pack_start(labelXp);

    // Initialisation des boutons dans la boîte horizontale
    buttonAjoutXp.set_label("Ajouter de l'expérience");
    buttonAjoutXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickAjoutXp));
    boxButton.pack_start(buttonAjoutXp);

    buttonRetraitXp.set_label("Retirer de l'expérience");
    buttonRetraitXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickEnleverXp));
    boxButton.pack_start(buttonRetraitXp);

    // Ajout des boutons à la boîte verticale
    box.pack_start(boxButton);
}

Interface::Interface(int width, int height, Perso& perso) : width(width), height(height), perso(&perso)
{
    // Initialisation de la fenêtre
    window.set_title("Interface");
    window.set_default_size(width, height);
    window.set_position(Gtk::WIN_POS_CENTER);
    window.set_border_width(10);

    // Initialisation de la boîte verticale
    box.set_orientation(Gtk::ORIENTATION_VERTICAL);
    box.set_spacing(10);
    window.add(box);

    // Initialisation de la boîte horizontale
    boxButton.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
    boxButton.set_spacing(10);

    // Initialisation des labels pour afficher les informations du personnage
    labelName.set_text("Nom : " + perso.getName());
    labelLevel.set_text("Niveau : " + to_string(perso.getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso.getCurrentXp()) + "/" + to_string(perso.getMaxXp()));
    box.pack_start(labelName);
    box.pack_start(labelLevel);
    box.pack_start(labelXp);

    // Initialisation des boutons dans la boîte horizontale
    buttonAjoutXp.set_label("Ajouter de l'expérience");
    buttonAjoutXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickAjoutXp));
    boxButton.pack_start(buttonAjoutXp);

    buttonRetraitXp.set_label("Retirer de l'expérience");
    buttonRetraitXp.signal_clicked().connect(sigc::mem_fun(*this, &Interface::clickEnleverXp));
    boxButton.pack_start(buttonRetraitXp);

    // Ajout des boutons à la boîte verticale
    box.pack_start(boxButton);
}

Interface::~Interface()
{
    
}

int Interface::getWidth()
{
    return width;
}

int Interface::getHeight()
{
    return height;
}

void Interface::setWidth(int width)
{
    this->width = width;
}

void Interface::setHeight(int height)
{
    this->height = height;
}

void Interface::drawBarreProgression()
{
    
}


void Interface::clickAjoutXp()
{
    perso->addXp(10);
    // Actualisation des labels
    labelLevel.set_text("Niveau : " + to_string(perso->getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso->getCurrentXp()) + "/" + to_string(perso->getMaxXp()));

    // Actualisation de la barre de progression
    barreProgression->update(perso->getCurrentXp(), perso->getMaxXp());
}

void Interface::clickEnleverXp()
{
    perso->removeXp(10);
    // Actualisation des labels
    labelLevel.set_text("Niveau : " + to_string(perso->getLevel()));
    labelXp.set_text("Expérience : " + to_string(perso->getCurrentXp()) + "/" + to_string(perso->getMaxXp()));

    // Actualisation de la barre de progression
    barreProgression->update(perso->getCurrentXp(), perso->getMaxXp());
}

void Interface::afficher()
{
    window.show_all_children();
    window.show();
    Main::run(window);
    window.hide();
}