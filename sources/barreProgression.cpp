/**
 * @file barreProgression.cpp
 * @author Lilian CHARDON (lilian.chardon@ynov.com)
 * @brief 
 * @version 0.1
 * @date 26-10-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "../headers/barreProgression.h"

/****************/
/* Constructeur */
/****************/

BarreProgression::BarreProgression() : width(0), height(0), value(0), maxValue(0){}

BarreProgression::BarreProgression(int width, int height) : width(width), height(height), value(0), maxValue(0){}

BarreProgression::BarreProgression(int width, int height, int value, int maxValue) : width(width), height(height), value(value), maxValue(maxValue){}

BarreProgression::~BarreProgression(){}

/************/
/* Méthodes */
/************/

bool BarreProgression::on_draw(const RefPtr<Context>& cr)
{
    Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();

    // Calcul de la taille de la barre
    double barWidth = width - 2 * BORDER_WIDTH;
    double barHeight = height - 2 * BORDER_WIDTH;

    // Calcul de la taille de la barre de progression
    double progressWidth = 0;
    if (value > 0)
        progressWidth = (barWidth - 2 * BORDER_WIDTH) * value / maxValue;
    double progressHeight = barHeight - 2 * BORDER_WIDTH;

    // Dessin de la barre
    cr->set_source_rgb(0.8, 0.8, 0.8);
    cr->rectangle(BORDER_WIDTH, BORDER_WIDTH, barWidth, barHeight);
    cr->fill();

    // Dessin de la barre de progression
    cr->set_source_rgb(0.2, 0.2, 0.2);
    cr->rectangle(BORDER_WIDTH + BORDER_WIDTH, BORDER_WIDTH + BORDER_WIDTH, progressWidth, progressHeight);
    cr->fill();

    return true;
}

void BarreProgression::update(int value, int maxValue)
{
    this->value = value;
    this->maxValue = maxValue;
    this->queue_draw();
}

/**********/
/* GETTER */
/**********/

int BarreProgression::getWidth() const
{
    return this->width;
}

int BarreProgression::getHeight() const
{
    return this->height;
}

int BarreProgression::getValue() const
{
    return this->value;
}

int BarreProgression::getMaxValue() const
{
    return this->maxValue;
}

/*
DrawingArea BarreProgression::getDrawingArea() const
{
    return this->drawingArea;
}
*/

/**********/
/* SETTER */
/**********/

void BarreProgression::setWidth(int width)
{
    this->width = width;
}

void BarreProgression::setHeight(int height)
{
    this->height = height;
}

void BarreProgression::setValue(int value)
{
    this->value = value;
}

void BarreProgression::setMaxValue(int maxValue)
{
    this->maxValue = maxValue;
}