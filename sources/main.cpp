#include <iostream>

#include "../headers/perso.h"
#include "../headers/interface.h"

int main(int argc, char *argv[])
{
    Main kit(argc, argv);
    Perso perso("Test", 0, 100, 0);
    Interface interface(perso);
    interface.afficher();

    /*
    cout << "Entrez le nom du personnage : ";
    string name;
    cin >> name;
    Perso perso2(name);
    perso2.displayPerso();
    
    while (1)
    {
        cout << "Ajouter de l'expérience ou diminuer le niveau ? (1/0) : ";
        int add_xp;
        int xp;
        cin >> add_xp;
        switch (add_xp)
        {
            case 0:
                cout << "Entrez le nombre de niveau à diminuer : ";
                cin >> xp;
                perso2.removeXp(xp);
                perso2.displayPerso();
                break;
            case 1:
                cout << "Entrez le nombre d'expérience à ajouter : ";
                cin >> xp;
                perso2.addXp(xp);
                perso2.displayPerso();
                break;
            
            default:
                return 0;
        }
    }
    */

    return 0;
}